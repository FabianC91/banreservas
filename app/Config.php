<?php 
	class Config{
		public $settings;

		public function __construct($request){
			$this->settings = $this->get_general_settings();

			switch($request){
				case 'database' :

				break;
			}
		}

		private function get_general_settings(){
			return array(
				'url_base' => '/banreservas',
				'url_sources' => '/banreservas/app/views/sources'
			);
		}

		private function get_settings_database(){
			$this->settings['host'] = '';
			$this->settings['user'] = '';
			$this->settings['name_database'] = '';
			$this->settings['password'] = '';
		}

		public function get_settings(){
			return $this->settings;
		}
	}
?>