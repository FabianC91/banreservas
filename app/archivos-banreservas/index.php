<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<?=$preheader?>
  <!--HEADER // -->
  <!--//BANNER -->
  <section>
    <div class="jumbotron jumbologin sliders" style="position:relative;">
      <div class="landing-container-princ">
        <div class="container">
          <div class="row">

            <!--Colum Der -->
            <div class="col-md-5 col-md-push-7 col-sm-12 col-xs-12 col-slider-de">
              <div  class="img-slider-prehome">
                <img src="<?php echo base_url().'sources/images/logoslider.png';?>" alt="">
              </div>

              <div class="titbanner1">
                <h2>Inicia sesi&oacute;n y <br>consulta tu meta</h2>
              </div>
              <div class="formlogin">
                <?php $attributes = array('class' => 'form-inline', 'id' =>'round');
                  echo form_open('preInit/guestStart',  $attributes); ?>
                  <div class="form-group formuDi">
                    <input type="number" class="form-control input-lg formdoc" id="numberdoc" name="numberdoc" placeholder="Documento de Identidad" data-validation="required number" data-validation-allowing="range[10000000;9999999999999999]">
                  </div>
                  <button type="submit" id="newPin" class="btn boton btn-default">Siguiente <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                  </button>
                <div class="btnform col-xss-12">
                  <span id="msjNRD" class="msj hidden">Datos no registrados en el sistema</span>
                </div>
                <?php echo form_close(); ?>
              </div>
            </div>

            <!--Colum izq -->
            <div class="col-md-7 col-md-pull-5 col-sm-12 col-xs-12 col-slider-iz">
              <div class="col-md-8 col-sm-8 col-xs-12 col-xss-12">
                <div class="contenedor-text-izq">
                  <?=$lista?>
                </div>
              </div>

              <div class="col-md-4 col-sm-4 hidden-xs">
                &nbsp;
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 callcenter" >
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <span style="font-weight:bold;"><img src="<?php echo base_url().'sources/images/iconphone.png';?>" alt="phone">&nbsp; En caso que no logre acceder a la plataforma</span>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <span style="float: left; padding: 0.5em 0em;">Telefono call center: 809-960-2121 Desde el interior sin cargos: 809-200-2131</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--// BANNER  -->
  <!--FOOTER // -->
  <?=$footer?>
</body>
</html>
<script>
$(document).ready(function(){
$("#noData").hide();

 $.validate({
    lang: 'es',
     validateOnBlur : false,
     errorMessagePosition : 'top', // Instead of 'inline' which is default
    scrollToTopOnError : false ,

  });
/*
  $("#newPin").on("clic", function(e) {
    var form = $("#round");

    e.preventDefault();

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('preInit/guestStart'); ?>",
        data: form.serialize(), // <--- THIS IS THE CHANGE
        dataType: "json",
        success: function(data){
          if (data.status == "success"){
            window.location.href = data.redirection;
          }else{
            $("#noData").show();
          }

        },
        error: function(data) {
          console.log(data);
          alert("Error posting feed."); }
   });
  });
*/

  $("#newPin").on("click", function(e) {
    var form = $("#round");

    e.preventDefault();

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('preInit/guestStart'); ?>",
        data: form.serialize(), // <--- THIS IS THE CHANGE
        dataType: "json",
        success: function(data){
          if (data.status == "success"){
            //window.location.href = data.redirection;
            //window.location.replace = data.redirection;
            $(location).attr('href', data.redirection)
          }else{
            //alert("Wait");
            //$('#numberdoc').tooltip('show');
            //$('#numberdoc').tooltip({'trigger':'manual', 'title': 'Datos no registrados'});
            //$("#noData").show();
            $("#msjNRD").removeClass("hidden");
          }

        },
        error: function(data) {
          console.log(data);
      //alert("Error requesting feed.");


        }
   });

  });


});




 </script>
