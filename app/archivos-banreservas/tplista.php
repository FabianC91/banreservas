<h1 class="tit-capan" style="color:#fff;font-size:25px;padding-left:0.5em;padding-bottom:0.5em;">Bienvenido a la campa&ntilde;a<br> que premiar&aacute; tus compras</h1>
<div class="list-number-iz col-md-12 col-sm-12 col-xs-12">
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2 num-list">
      <span>1.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list1">
      <span>Realiza todas tu compras con tus<br> <strong>Tarjetas Mastercard Banreservas</strong>.</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2 num-list">
    <span>2.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list2">
      <span >Tienes una meta asignada cada semana<br> que puedes cumplir acumulando compras<br> en diferentes comercios y usando tu tarjeta<br> al menos 1 vez en la categoría de la semana.</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2 num-list">
    <span>3.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list3">
      <span >Al cumplir tu meta elige tu premio ingresando<br> con tu documento de indentidad y los &uacute;ltimos<br> 4 dígitos de tu <strong>Tarjeta Mastercard Banreservas.</strong> <br>Canjea tu premio en la siguiente semana.</span>
    </div>
  </div>
</div>
