<?php include './app/views/subtemplates/preheader.php'; ?>
  <!--HEADER // -->
  <!--//BANNER -->
  <section>
    <div class="jumbotron jumbologueo sliders">
        <div class="container">
          <div class="row">

            <div class="col-md-5 col-md-push-7 col-sm-12 col-xs-12 col-slider-der">
                <div  class="img-slider-prehome">
                  <img src="<?php echo $url_sources ?>/images/logoslider.png" alt="">
                </div>
                <div class="titbanner2">
                  <h2>Inicia sesión y conoce los premios que puedes canjear</h2>
                </div>

                <div class="formlogin1">
                  <form action="<?php echo $url_base ?>/home" method="post" class="form-horizontal form2">
                    <div class="form-group">
                      <div class="col-sm-12 col-xs-12 input-tarje">
                        <input type="password" class="form-control input-lg formdoc" id="numberdoc" placeholder="*** Últimos 4 digitos de tu Tarjeta Mastercard Banreservas">
                      </div>
                      <div class="col-sm-12 col-xs-12">
                        <span class="info1">*Puedes ingresar cualquiera de tus Tarjetas Mastercard Banreservas.</span>
                        <span id="msj" class="msjError ">El n&uacute;mero ingresado no es valido</span>

                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-12 col-xs-12 contentcheck">
                        <div class="checkbox terminoslogueado col-sm-7 col-xs-12">
                          <label>
                            <input type="checkbox"> <a class="termin" data-toggle="modal" data-target="#modal-terminos" href="#" >Acepto T&eacute;rminos y condiciones</a>
                          </label>
                        </div>
                        <div class="col-sm-offset-2 col-sm-5 col-xs-12 contentboton">
                          <button type="submit" class="btn boton btn-default">Iniciar <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                          </button>
                        </div>
                      </div>
                    </div>

                  </form>
                </div>

            </div>

            <div class="col-md-7 col-md-pull-5 col-sm-12 col-xs-12 col-slider-iz">
              <div class="col-md-8 col-sm-8 col-xs-12 col-xss-12">
                <div class="contenedor-text-izq">
                  <?php include 'app/views/blocks/tplista.php'; ?>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-6">
                <!-- <img class="man" src="<?php echo $url_sources ?>/images/person3.png" alt="man"> -->
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>
  <!--// BANNER  -->
  <!--FOOTER // -->
<?php include 'app/views/subtemplates/footer.php'; ?>

</body>
</html>
