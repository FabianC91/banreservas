<?php include './app/views/subtemplates/preheader.php'; ?>
  <!--HEADER // -->
  <!--//BANNER -->
  <section>
    <div class="jumbotron jumbologin sliders" style="position:relative;">
      <div class="container">
        <div class="row">
          <!--Colum Der -->
          <div class="col-md-5 col-md-push-7 col-sm-12 col-xs-12 col-slider-de">
            <div  class="img-slider-prehome">
              <img src="<?php echo $url_sources ?>/images/logoslider.png" alt="">
            </div>
            <div class="titbanner1">
              <h2>Inicia sesión y conoce los premios que puedes canjear</h2>
            </div>
            <div class="formlogin">
              <form action="<?php echo $url_base ?>/clave" method="post" class="form-inline">
                <div class="form-group formuDi">
                  <input type="number" class="form-control input-lg formdoc" id="numberdoc" placeholder="Documento de Identidad">
                </div>

                <button type="submit" class="btn boton btn-default">Siguiente <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </button>
              </form>
            </div>
            <span id="msj" class="msjError hidden">El n&uacute;mero ingresado no es valido</span>
          </div>

          <!--Colum izq -->
          <div class="col-md-7 col-md-pull-5 col-sm-12 col-xs-12 col-slider-iz">
            <div class="col-md-8 col-sm-8 col-xs-12 col-xss-12">
              <div class="contenedor-text-izq">
                <?php include 'app/views/blocks/tplista.php'; ?>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 hidden-xs">
              &nbsp;
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 callcenter" style="background-color: #f89420; color: #fff; position: absolute; left: 0; text-align: center; padding: 0.5em 0em; bottom: 0; box-shadow: 0px -6px 14px 0px #00000047;-moz-box-shadow:0px -6px 14px 0px #00000047;-webkit-box-shadow:0px -6px 14px 0px #00000047; ">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <span style="font-weight:bold;"><img src="<?php echo $url_sources ?>/images/iconphone.png" alt="phone">&nbsp; Ante cualquier inconveniente contáctanos llamando a los teléfonos</span>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <span style="padding:1%;display:block;">809 960 2121 y desde el interior sin cargos al 809 200 2131</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--// BANNER  -->
  <!--FOOTER // -->
  <?php include './app/views/subtemplates/footer.php'; ?>

</body>
</html>
