<?php include 'app/views/subtemplates/header.php'; ?>
  <section id="mimeta">
    <div class="jumbotron slider-interno jumbointerno">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 col-xss-12 col-iz">
            <div class="col-md-5 col-sm-5 col-xs-5 col-xss-12">
              <div class="smartphone">
                <img src="<?php echo $url_sources ?>/images/smartphone.png" alt="smartphone">
              </div>
              <div class="content-text-left">
                <h2>Semana </h2>
                <h3>Meta de Tecnolog&iacute;a</h3>
              </div>

            </div>
            <div class="col-md-7 col-sm-7 col-xs-7 col-xs-12 content-right-text">
              <div class="tit-text-right">
                <h2>Hola,</h2>
                <h3>Juan Fernando Zuluaga</h3>
              </div>
              <div class="parraf-slider">
                <h4>Meta semana </h4>
                <!-- <p>Para cumplir tu meta recuerda realizar al menos una<br> transacci&oacute;n en la categoria de tecnología y acumula<br> compras por RD$XXX en diferentes establecimientos<br> comerciales</p> -->
                <p>Para cumplir tu meta debes acumular compras con tu <strong>Tarjeta Mastercard Banreservas</strong> por <strong>RD$XXX.00</strong> en diferentes comercios y recuerda que debes realizar al menos una compra en <strong>Tecnolog&iacute;a.</strong></p>
                <p>Esta meta va del 26 noviembre al 3 de diciembre. </p>
              </div>
              <div class="">
                <!-- <a class="conoce" data-toggle="modal" data-target="#myModal" href="#">Conoce qu&eacute; incluye esta categoria </a> -->
              </div>
              <div class="content-btn ">
                <a class="btn-banner btn-block bgFuchsia not-active" type="submit"><img src="<?php echo $url_sources ?>/images/locked.png" alt="">  A&uacute;n no puedes canjear este premio</a>
              </div>
              <div class="parraf-info">
                <p>* Este bot&oacute;n se iluminar&aacute; al cumplir la meta.</p>
                <p>* Si eres ganador, podrás canjear tu premio <br><strong>7 días después</strong> de que finalice el reto.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-5 visible-lg">
            <!-- <img class="man2" src="<?php echo $url_sources ?>/images/person2.png" alt=""> -->
          </div>
        </div>
      </div>
    </div>

  </section>
  <!--Seccion de metas semanales -->
  <section class="allmetas">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-xss-12 containermetas">
          <div class="titlemetas">
            <h2>Todas <br />las <br /> metas</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <?php include 'app/views/blocks/metas.php'; ?>
      </div>
    </div>
  </section>
  <!--Catalogo de Productos -->
  <section id="premios" class="CatPremios">
    <div class="container">
      <div class="landing-container premios">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 col-xss-12">
            <div class="titleprem">
              <h2>Cat&aacute;logo<br />de<br />premios</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <?php include 'app/views/blocks/cat-premios.php'; ?>
        </div>
      </div>
    </div>
  </section>
  <section id="recomendacion" class="Historial-red">
    <div class="container">
      <div class="landing-container premios">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 col-xss-12">
            <div class="titleHprem">
              <h2>historial<br />de<br />Premios</h2>
            </div>
          </div>
        </div>
        <!--Cuadro Sin Historial -->
        <div class="sinhistory">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xss-12 DialogHistory">
              <h2>¡A&uacute;n no has canjeado<br /> ning&uacute;n premio!</h2>
              <img src="<?php echo $url_sources ?>/images/alarm.png" alt="">

            </div>
            <div class="rombo2"></div>
          </div>
        </div>

        <!--Productos Historial -->
        <div class="products hidden">
          <div class="row">
            <?php include 'app/views/historial-premios.php' ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- -->
  <?php include 'app/views/subtemplates/footer.php';?>

  <!--MODAL -->
  <!-- Modal 1-->
  <div class="modal fade " id="myModal" role="dialog">
    <div class="modal-dialog1">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button id="cerrarmodal" type="button" class="close">cerrar</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-7 col-sm-6 col-xs-6 Titlemodal1 azul">
              <h2>¿Qu&eacute; puedes comprar en <br />esta categor&iacute;a?</h2>
              <ul class="listmodal1">
                <li>Compra de computadoras.</li>
                <li>Reparaci&oacute;n de computadoras, televisores, celulares.</li>
                <li>Compra de Software.</li>
                <li>Actualizaci&oacute;n de Software.</li>
                <li>Pago de sistemas IOT</li>
              </ul>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-6 imgmodal">
              <img src="<?php echo $url_sources ?>/images/imgModalCompras.png" alt="">
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
  <!--Modal para productos -->
  <!-- Modal -->
  <?php include 'app/views/subtemplates/modal-prem.php'; ?>




  <!--Script Modal -->

</body>

</html>
