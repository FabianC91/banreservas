<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<?=$header?>
<section id="mimeta">
  <div class="jumbotron jumboallpremios">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 col-xss-12 col-slider-de">
          <div class="col-md-5 col-sm-5 col-xs-5 col-xss-12">
            <div class=" BurPremio">
              <img src="<?php echo base_url().'sources/images/iggenerico.png';?>" alt="">
            </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-7 col-xs-12 content-right-text">
            <div class="tit-text-right">
              <h2>Cat&aacute;logo de <br>premios </h2>
            </div>
          </div>
        </div>
        <div class="col-lg-5 visible-lg">

        </div>
      </div>
    </div>
  </div>

</section>
<section id="premios" class="CatPremios">
  <div class="container">
<div class="cat-prem">
  <?php foreach($items as $post){?>
     <div class="list-prem">
        <div class="items">
          <div class="contnt-btnmas">
            <a href="#" data-toggle="modal" data-target="#ModalProduct<?php echo $post['id'];?>"><img class="btnmas" src="<?php echo base_url().'sources/images/btnmas.png';?>" alt=""></a>
          </div>
          <div class="product-thumbnail more-prem">
            <img src="<?php echo base_url().'sources/images/'.$post['image_url'];?>" alt="Image">
          </div>
        </div>
        <div class="">
        <h3 class="titlecatalogo"><?php echo $post['name'];?></h3>
      </div>
    </div>
  <?php }?>
</div>
</div>
</section>
 <?=$footer?>
<?=$supermodals?>
