<?php include 'app/views/subtemplates/header.php'; ?>

<section id="mimeta">
  <div class="jumbotron jumboallpremios">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 col-xss-12  col-slider-de">
          <div class="col-md-5 col-sm-5 col-xs-5 col-xss-12">
            <div class=" BurPremio">
              <img src="<?php echo $url_sources ?>/images/iggenerico.png" alt="">
            </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-7 col-xs-12 content-right-text">
            <div class="tit-text-right">
              <h2>Cat&aacute;logo de <br>premios </h2>
            </div>
          </div>
        </div>
        <div class="col-lg-5 visible-lg">

        </div>
      </div>
    </div>
  </div>

</section>

<section id="premios" class="CatPremios">
  <div class="container">
    <div class="landing-container premios">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 col-xss-12">

        </div>
      </div>
      <div class="row">
        <?php include 'app/views/blocks/all-premios.php'; ?>
      </div>
    </div>
  </div>
</section>


<?php include 'app/views/subtemplates/modal-prem.php'; ?>

  <?php include 'app/views/subtemplates/footer.php';?>


<!--Script Modal -->

</body>

</html>
