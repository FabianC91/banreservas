<?php include 'app/views/subtemplates/header.php'; ?>
  <section>
    <div class="jumbotron slider-interno jumbointerno2">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-12 col-xs-12 col-xss-12">
            <div class="col-md-7 col-sm-6 col-xs-6">
              <div class="contenedor-premio">
                <div class="items-prem">
                  <div class="product-thumbnail more-prem">
                    <img src="<?php echo $url_sources ?>/images/iphonex.png" alt="Image">
                  </div>
                </div>
              </div>

            </div>
            <div class="col-md-5 col-sm-6 col-xs-6 col-xss-12 content-right-text2">
              <div class="tit-text-right2">
                <h3>Este es el premio que<br />vas a canjear</h3>
              </div>
              <div class="line-right">
                <svg  height="26" width="250" style="stroke: #fff; stroke-width: 2;">
                  <line class="line1" x1="0" y1="16" x2="70" y2="16" />
                </svg>
              </div>
              <div class="parraf-slider2">
                <p>Lorem ipsum dolor sit amet,
                  consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                  <br />
              <span>Correspondiente a la<br />
              Meta 1 de Tecnolog&iacute;a</span>
              </div>
            </div>
          </div>
          <div class="col-md-4 hidden-sm hidden-xs">
            <!-- <img class="girl2" src="<?php echo $url_sources ?>/images/girl2.png" alt=""> -->
          </div>
        </div>
      </div>
    </div>

  </section>
  <section class="backform">
    <div class="container">
      <div class="landing-container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 containermetas">
            <form>
            <div class="titledatos">
              <h2>Datos personales</h2>
              <span class="obligatorio">(*)campos obligatorios</span>
            </div>
            <div class="formdatos">
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputEmail1">Primer Nombre</label><span class="obligatorio">*</span>
                  <input type="text" class="form-control personal colorplace" id="inputprimernombre" placeholder="Andr&eacute;s">
                </div>
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputPassword1">Primer Apellido</label><span class="obligatorio">*</span>
                  <input type="text" class="form-control personal colorplace" id="inputprimerapellido" placeholder="L&oacute;pez">
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputEmail1">Segundo Nombre</label>
                  <input type="text" class="form-control personal colorplace" id="InputSegundoNombre" placeholder="Felipe">
                </div>
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputPassword1">Segundo Apellido</label>
                  <input type="text" class="form-control personal colorplace" id="InputSegundoApellido" placeholder="G&oacute;mez">
                </div>
              </div>

            </div>
            <div class="titleformcontac">
              <div class="titlecontacto " >
                <h2>Datos de contacto</h2>
                <span>Tus Datos de contacto son muy importantes para poder entregarte los premios redimidos.</span>
              </div>
            </div>
            <div class="formcontact col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputEmail1">Tel&eacute;fono fijo</label><span class="obligatorio">*</span>
                  <input type="number" class="form-control inputcontact" id="fijo" placeholder="536 78 239">
                  <span id="msj" class="msj">El n&uacute;mero ingresado no es valido</span>
                </div>
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputEmail1">Email</label><span class="obligatorio">*</span>
                  <input type="email" class="form-control inputcontact" id="exampleInputEmail1" placeholder="Email@email.com">
                  <span id="msj" class="msj hidden">El n&uacute;mero ingresado no es valido</span>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputPassword1">M&oacute;vil</label><span class="obligatorio">*</span>
                  <input type="number" class="form-control inputcontact" id="movil" placeholder="3113000000">
                  <span id="msj" class="msj hidden">El n&uacute;mero ingresado no es valido</span>
                </div>
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputPassword1">Distrito</label><span class="obligatorio">*</span>
                <select class="form-control inputcontact">
                  <option disabled="disabled" selected="selected">Seleccione</option>
                  <option value="Barahona">Barahona</option>
                  <option value="Indepencia">Indepencia</option>
                  <option value="Bahoruco">Bahoruco</option>
                  <option value="Pedernales">Pedernales</option>
                  <option value="Nacional">Nacional</option>
                  <option value="La Vega">La Vega</option>
                  <option value="Constanza">Constanza</option>
                  <option value="Monseñor Nouel">Monseñor Nouel</option>
                  <option value="Espaillat">Espaillat</option>
                  <option value="Sánchez Ramírez">Sánchez Ramírez</option>
                  <option value="Montecristi">Montecristi </option>
                  <option value="Santiago Rodríguez">Santiago Rodríguez</option>
                  <option value="Dajabón">Dajabón</option>
                  <option value="San Cristóbal">San Cristóbal</option>
                  <option value="Baní">Baní</option>
                  <option value="Azua">Azua</option>
                  <option value="San José de Ocoa">San José de Ocoa</option>
                  <option value="Villa Altagracia">Villa Altagracia</option>
                  <option value="Puerto Plata">Puerto Plata</option>
                  <option value="Santo Domingo">Santo Domingo</option>
                  <option value="Monte Plata">Monte Plata</option>
                  <option value="Duarte (San Francisco de Macorís)">Duarte (San Francisco de Macorís)</option>
                  <option value="Salcedo">Salcedo</option>
                  <option value="María Trinidad Sánchez">María Trinidad Sánchez</option>
                  <option value="Samaná">Samaná</option>
                  <option value="San Juan de la Maguana">San Juan de la Maguana</option>
                  <option value="Elías Piña">Elías Piña</option>
                  <option value="San Pedro de Macorís">San Pedro de Macorís</option>
                  <option value="El Seybo">El Seybo</option>
                  <option value="Hato Mayor">Hato Mayor</option>
                  <option value="La Romana">La Romana</option>
                  <option value="La Altagracia (Higüey)">La Altagracia (Higüey)</option>
                  <option value="Santiago">Santiago</option>
                  <option value="Valverde Mao">Valverde Mao</option>

                </select>
                <span id="msj" class="msj hidden">El n&uacute;mero ingresado no es valido</span>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="tituloform azul" for="exampleInputPassword1">Direcci&oacute;n</label><span class="obligatorio">*</span>
                  <input type="text" class="form-control inputcontact" id="exampleInputPassword1" placeholder="xxx xxx xxx...">
                  <span id="msj" class="msj">El n&uacute;mero ingresado no es valido</span>
                </div>

              </div>
          </div>
          <div class="btnform col-xss-12">
            <div class="checkbox">
              <label>
                <input type="checkbox"> <a href="#" target="_blank">Acepto los T&eacute;rminos y condiciones de uso</a>
              </label>
            </div>
            <div class="boton2">
              <button type="submit" data-toggle="modal" data-target="#ModalRedencion" class="btn btnpuntos btn-default">canjear premio</button>
            </div>

          </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <button type="submit" class="btn btnpuntos btn-default hidden" data-toggle="modal" data-target="#ModalRedencion">canjear premio</button>
        </div>
      </div>
    </div>
  </section>
  <!--FOOTER // -->
  <?php include 'app/views/subtemplates/footer.php'; ?>
  <!--//FOOTER -->

  <!--MODAL -->
  <!--Modal para productos -->
  <!-- Modal -->
    <div class="modal fade" id="ModalRedencion" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">cerrar</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-7 Titlemodal3 azul">
                <h2>¡Tu canje se ha <br />realizado exitosamente!</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quia cupiditate ab quisquam dignissimos reiciendis, laboriosam, magnam nostrum eius. </p>
                <h4>Califica tu experiencia</h4>
                <div class="col-md-12 col-sm-12 col-xs-12 clasificacion">
                <div class="ec-stars-wrapper">
                  <a href="#" data-value="1" title="Votar con 1 estrellas">★</a>
                  <a href="#" data-value="2" title="Votar con 2 estrellas">★</a>
                  <a href="#" data-value="3" title="Votar con 3 estrellas">★</a>
                  <a href="#" data-value="4" title="Votar con 4 estrellas">★</a>
                  <a href="#" data-value="5" title="Votar con 5 estrellas">★</a>
                </div>
              </div>
                <button id="ShopCar"  data-dismiss="modal" class="btn btn-default Shopcar-exit" type="submit">Cerrar &nbsp;&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-5 imgmodalredencion">
                <img src="<?php echo $url_sources ?>/images/imgRedencionModal.png" alt="">
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>

</body>
</html>
