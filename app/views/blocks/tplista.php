<h1 class="tit-capan" >Ganar con Mastercard y Banreservas es muy fácil</h1>
<div class="list-number-iz col-md-12 col-sm-12 col-xs-12">
  <div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1 num-list">
      <span>1.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list1">
      <span>Conoce tu reto con tu meta semanal ingresando al sitio con tu documento de identidad y los últimos 4 dígitos de tu <strong>Tarjeta Mastercard Banreservas</strong>. </span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1 num-list">
    <span>2.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list2">
      <span >Usa tu <strong>Tarjeta Mastercard Banreservas</strong> en todas tus compras y cumple tu meta.</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1 num-list">
    <span>3.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list3">
      <span >Canjea un premio y vuelve a <strong>¡ganar!</strong> </span>
    </div>
  </div>
</div>
<!-- <strong>Tarjetas Mastercard Banreservas</strong> -->
