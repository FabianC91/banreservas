<h1 class="tit-lista">Bienvenido al lugar<br> que premia tus compras</h1>
<div class="list-number-iz col-md-12 col-sm-12 col-xs-12">
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2 num-list">
      <span>1.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list1">
      <span >Haz tus compras con tus <strong>Tarjetas<br> Mastercard Banreservas</strong>.</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2 num-list">
    <span>2.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list2">
      <span>Cada semana tenemos una categor&iacute;a<br> diferente para ti. Acumula tus compras<br> y alcanza tu meta.</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2 num-list">
    <span>3.</span>
    </div>
    <div class="col-md-10 col-sm-10 col-xs-10 textlist list3">
      <span >Al cumplir tu meta ingresa con tu documento<br> de identidad y los últimos 4 d&iacute;gitos de tu<br>
<strong>Tarjeta Mastercard Banreservas.</strong><br> Canjea tu premio en la siguiente semana.</span>
    </div>
  </div>
</div>
