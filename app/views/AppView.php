<?php
	class AppView extends GeneralView{
		public function __construct(){
			parent::__construct();
		}

		public function print_indentidad(){
			$this->get_template('index');
		}

		public function print_clave(){
			$this->get_template('logueado');
		}

		public function print_home(){
			$this->get_template('home');
		}

		public function print_premios(){
			$this->get_template('premios');
		}

		public function print_contacto(){
			$this->get_template('contacto');
		}
		public function print_catalogo(){
			$this->get_template('catalogo');
		}
	}
?>
