<div class="modal fade " id="modal-preguntas" role="dialog">
  <div class="modal-dialog1">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="cerrarmodal" type="button" class="close">cerrar</button>
      </div>
      <div class="modal-body body-preg">
        <div class="terminos">
          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{22}" paraid="1088544284">Preguntas Frecuentes&nbsp;</p>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{28}" paraid="873635521">&nbsp;</p>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{32}" paraid="1231106048">PARA CONOCER&nbsp;T&Eacute;RMINOS Y CONDICIONES, INGRESE&nbsp;A&nbsp;LA PLATAFORMA&nbsp;&nbsp;&nbsp;<br />
          www.usalaygana.com&nbsp;</p>

          <ol role="list" start="1">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="1" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{62}" paraid="194945618">&iquest;Qu&eacute;&nbsp;es&nbsp;&ldquo;&Uacute;sala y Gana&rdquo;?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{79}" paraid="1107495382">Es una campa&ntilde;a creada por&nbsp;Banreservas&nbsp;y&nbsp;Mastercard&nbsp;que promueve el uso&nbsp;de sus Tarjetas&nbsp;D&eacute;bito&nbsp;y&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;para&nbsp;un grupo de clientes definido, premi&aacute;ndolos por alcanzar su Meta Personal de Facturaci&oacute;n, la cual&nbsp;podr&iacute;a tambi&eacute;n&nbsp;estar&nbsp;compuesta por una cantidad de categor&iacute;as de comercio a activar&nbsp;cada semana.&nbsp;Se debe ingresar a la plataforma para verificar las instrucciones de juego.&nbsp;&nbsp;</p>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{129}" paraid="1472031197">&nbsp;</p>

          <ol role="list" start="2">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="2" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{133}" paraid="161298356">&iquest;Qui&eacute;nes pueden participar en&nbsp;la&nbsp;campa&ntilde;a&nbsp;&Uacute;sala y Gana?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{148}" paraid="1869523331">Titulares,&nbsp;personas naturales que tengan como m&iacute;nimo una cuenta de Ahorros o Corriente y que a su vez,&nbsp;su medio de manejo de &eacute;stos sea a trav&eacute;s de Tarjeta D&eacute;bito&nbsp;y/o Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas, los cu&aacute;les ser&aacute;n seleccionados previamente por el Banco&nbsp;&nbsp;Banreservas, y comunicados sobre su participaci&oacute;n a trav&eacute;s de correo electr&oacute;nico, SMS y/o Correo Directo.&nbsp;&nbsp;</p>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{180}" paraid="1697837701">&nbsp;</p>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{184}" paraid="57278075">No es necesario inscribirse para&nbsp;participar, pero s&iacute; deber&aacute;&nbsp;ingresar a la plataforma&nbsp;www.usalaygana.com&nbsp;para conocer la Meta Personal de Facturaci&oacute;n, conocer los&nbsp;diferentes&nbsp; premios, y como canjearlos;&nbsp;lo cual podr&aacute; realizar en cualquier momento durante la Vigencia de la campa&ntilde;a&nbsp;al alcanzar su meta semanal.&nbsp;</p>

          <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{214}" paraid="341072327">&nbsp;</p>

          <ol role="list" start="3">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="3" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{218}" paraid="50648935">&iquest;C&oacute;mo participar si fue seleccionado?&nbsp;</p>
            </li>
          </ol>

          <ol role="list" start="1">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="1" data-font="" data-leveltext="%1." data-listid="13" role="listitem">
            <p paraeid="{5d876343-6798-4049-9188-85ca731ea986}{225}" paraid="701994887">Ingresar a&nbsp;www.usalaygana.com&nbsp;con los d&iacute;gitos de su c&eacute;dula y los &uacute;ltimos 4 d&iacute;gitos de cualquiera de sus&nbsp;Tarjetas&nbsp;D&eacute;bito&nbsp;y/o&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas.&nbsp;</p>
            </li>
          </ol>

          <ol role="list" start="2">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="2" data-font="" data-leveltext="%1." data-listid="13" role="listitem">
            <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{9}" paraid="1323099837">Al&nbsp;Participante&nbsp;se le ha asignado una meta de facturaci&oacute;n. En algunos casos, el cliente tendr&aacute; una meta adicional&nbsp;la cual&nbsp;est&aacute;&nbsp;compuesta por una categor&iacute;a de comercio a activar.&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{28}" paraid="413937394">&nbsp;</p>

          <ol role="list" start="3">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="3" data-font="" data-leveltext="%1." data-listid="13" role="listitem">
            <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{32}" paraid="193778318">El&nbsp;participante&nbsp;debe realizar sus compras con sus Tarjetas D&eacute;bito&nbsp;y/o&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;para acumular la facturaci&oacute;n y llegar al monto de facturaci&oacute;n fijado como meta.&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{59}" paraid="2103298570">&nbsp;</p>

          <ol role="list" start="4">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="4" data-font="" data-leveltext="%1." data-listid="13" role="listitem">
            <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{63}" paraid="880167081">Una vez el&nbsp;participante&nbsp;llegue a su meta, el&nbsp;participante&nbsp;podr&aacute; ingresar al Cat&aacute;logo de Premios en&nbsp;www.usalaygana.com&nbsp;y&nbsp;canjear el premio que le guste.&nbsp;&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{92}" paraid="1541802816">&nbsp;</p>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{96}" paraid="1356240608">5. En algunos casos, el&nbsp;cliente&nbsp;recibir&aacute; una categor&iacute;a de comercio diferente cada semana.&nbsp;Todos podr&aacute;n&nbsp;cumplir la meta 8 veces durante la campa&ntilde;a y&nbsp;canjear&nbsp;8 premios al alcanzarla respectivamente (1 por semana).&nbsp;&nbsp;</p>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{116}" paraid="389651857">&nbsp;</p>

          <ol role="list" start="4">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="4" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{120}" paraid="1399255777">&iquest;Cu&aacute;les son los premios?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{127}" paraid="1609443706">Premios generales&nbsp;(Catalogo de Premios):&nbsp;Son los premios que los&nbsp;participantes&nbsp;podr&aacute;n&nbsp;canjear&nbsp;al cumplir su meta personal.&nbsp;Podr&aacute;n canjear 8 premios al alcanzarla respectivamente (1 por semana).&nbsp;&nbsp;</p>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{153}" paraid="1395403733">&nbsp;</p>

          <ol role="list" start="5">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="5" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{157}" paraid="1620728902">&iquest;Cu&aacute;l es la mec&aacute;nica de la&nbsp;Campa&ntilde;a&nbsp;&Uacute;sala y Gana?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{80c64bd8-8794-4a0f-a2c1-d7bed227e6b5}{172}" paraid="589549449">La&nbsp;Campa&ntilde;a&nbsp;consiste en premiar a los&nbsp;Participantes&nbsp;que&nbsp;m&aacute;s facturen, con cualquiera o varias de sus&nbsp;Tarjetas&nbsp;D&eacute;bito&nbsp;y/o&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;de acuerdo&nbsp;con&nbsp;la Meta Personal de Facturaci&oacute;n&nbsp;y/o&nbsp;categor&iacute;a&nbsp;establecida&nbsp;por el banco para cada&nbsp;participante, la cual podr&aacute; consultar en la Plataforma. Si cumplen con&nbsp;su Meta Personal&nbsp;podr&aacute;n&nbsp;redimir&nbsp;Premios&nbsp;en el cat&aacute;logo de&nbsp;&Uacute;sala&nbsp;y Gana.&nbsp;&nbsp;</p>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{1}" paraid="1622109301">&nbsp;</p>

          <ol role="list" start="6">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="6" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{5}" paraid="2145852180">&iquest;Qu&eacute; es la Meta Personal de Facturaci&oacute;n?&nbsp;<br />
            La Meta Personal de Facturaci&oacute;n est&aacute;&nbsp;conformada&nbsp; de&nbsp;un consumo m&iacute;nimo que debe facturar el Participante para&nbsp;activar la redenci&oacute;n de premios.&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{18}" paraid="556413568">&nbsp;</p>

          <ol role="list" start="7">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="7" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{22}" paraid="873184052">&nbsp;&iquest;Qu&eacute; es la Meta Personal de Facturaci&oacute;n&nbsp;y&nbsp;categor&iacute;a&nbsp;de comercio&nbsp;asignada?&nbsp;&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{41}" paraid="1188348247">La Meta Personal de Facturaci&oacute;n&nbsp;y&nbsp;categor&iacute;a&nbsp;de&nbsp;comercio&nbsp;forman parte de&nbsp;la meta&nbsp;que asigna el banco para&nbsp;algunos&nbsp;participantes, y est&aacute;&nbsp;conformada&nbsp;&nbsp;de&nbsp;un consumo m&iacute;nimo que debe facturar el Participante&nbsp;para activar la redenci&oacute;n de premios y una m&iacute;nima categor&iacute;a en la que debe realizar transacciones para habilitar la redenci&oacute;n semanalmente.&nbsp;&nbsp;</p>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{77}" paraid="1795822630">&nbsp;</p>

          <ol role="list" start="8">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="8" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{81}" paraid="2092164370">&iquest;Cu&aacute;les son las Transacciones v&aacute;lidas que suman a mi Meta Personal de Facturaci&oacute;n&nbsp;y/o&nbsp;&nbsp;categor&iacute;a&nbsp;de comercio&nbsp;asignado?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{104}" paraid="476567988">&Uacute;nicamente ser&aacute;n v&aacute;lidas las compras en los establecimientos de comercio realizadas&nbsp;a trav&eacute;s de&nbsp;dat&aacute;fonos&nbsp;con cualquiera de sus Tarjetas D&eacute;bito y/o&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas.&nbsp;Es decir, solo participan las compras en POS a nivel nacional&nbsp;e internacional, no se consideran pagos de servicios ni transacciones en agencias, cajeros autom&aacute;ticos (ATM),&nbsp;recargas de tarjeta transporte,&nbsp;transferencias o traslados.&nbsp;Para conocer m&aacute;s sobre las Transacciones v&aacute;lidas para la&nbsp;Campa&ntilde;a, dir&iacute;jase a los t&eacute;rminos y condiciones.&nbsp;</p>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{154}" paraid="1461162777">&nbsp;</p>

          <ol role="list" start="9">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="9" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{158}" paraid="1047729555">&iquest;C&oacute;mo me inscribo?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{165}" paraid="1611631880">El&nbsp;Cliente&nbsp;&nbsp;no&nbsp;necesita inscribirse para&nbsp;participar.&nbsp;Sin embargo, el participante debe&nbsp;ingresar&nbsp;a&nbsp;la Plataforma&nbsp;a trav&eacute;s de&nbsp;www.usalaygana.com&nbsp;para&nbsp;redimir premios,&nbsp;conocer su Meta Personal de Facturaci&oacute;n,&nbsp;y/o&nbsp;las categor&iacute;as&nbsp;que debe activar.&nbsp;&nbsp;</p>

          <p paraeid="{81d75229-9501-4a30-8e70-370674acee5e}{233}" paraid="1166446895">Para&nbsp;ingresar a&nbsp;la&nbsp;p&aacute;gina&nbsp;web&nbsp;www.usalaygana.com,&nbsp;el&nbsp;participante&nbsp;deber&aacute; autenticarse&nbsp;con&nbsp;los&nbsp;d&iacute;gitos&nbsp;de su c&eacute;dula&nbsp;y los &uacute;ltimos 4 d&iacute;gitos de su&nbsp;Tarjeta&nbsp;D&eacute;bito&nbsp;y/o Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas.&nbsp;</p>

          <ol role="list" start="10">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="10" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{40}" paraid="520562128">&iquest;C&oacute;mo&nbsp;canjeo un premio?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{51}" paraid="955154594">Para&nbsp;redimir&nbsp;uno de los&nbsp;premios,&nbsp;el participante debe:&nbsp;&nbsp;</p>

          <ol role="list" start="1">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="1" data-font="" data-leveltext="%1." data-listid="10" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{69}" paraid="1057900820">Ingresar al sitio web&nbsp;www.usalaygana.com&nbsp;&nbsp;&nbsp;</p>
            </li>
          </ol>

          <ol role="list" start="2">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="2" data-font="" data-leveltext="%1." data-listid="10" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{90}" paraid="1259788781">Consultar en el cat&aacute;logo de productos la disponibilidad de los premios.&nbsp;</p>
            </li>
          </ol>

          <ol role="list" start="3">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="3" data-font="" data-leveltext="%1." data-listid="10" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{101}" paraid="1612494307">Validar&nbsp;si tiene un premio listo para&nbsp;canjear.&nbsp;&nbsp;</p>
            </li>
          </ol>

          <ol role="list" start="4">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="4" data-font="" data-leveltext="%1." data-listid="10" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{116}" paraid="2124707289">Seleccionar el producto a&nbsp;canjear.&nbsp;&nbsp;</p>
            </li>
          </ol>

          <ol role="list" start="5">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="5" data-font="" data-leveltext="%1." data-listid="10" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{129}" paraid="2025687822">Si cuenta&nbsp;con&nbsp;la opci&oacute;n de canje activo,&nbsp;la plataforma le&nbsp;permitir&aacute; realizar&nbsp;el canje.&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{154}" paraid="1981459121">&nbsp;</p>

          <ol role="list" start="11">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="11" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{158}" paraid="1963857513">&iquest;Cu&aacute;ntas&nbsp;Transacciones se deben hacer&nbsp;los Participantes&nbsp;para&nbsp;activar&nbsp;la meta de facturaci&oacute;n?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{179}" paraid="851530157">Es&nbsp;necesario cumplir con el m&iacute;nimo de facturaci&oacute;n asignado, sin importar el n&uacute;mero de&nbsp;Transacciones.&nbsp;&nbsp;</p>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{191}" paraid="955765370">&nbsp;</p>

          <ol role="list" start="12">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="12" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{195}" paraid="374495214">&iquest;Cu&aacute;ntas Transacciones se deben hacer los Participantes para activar&nbsp;la meta de facturaci&oacute;n y categor&iacute;a asignada en la semana?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{206}" paraid="1879315835">Es&nbsp;necesario cumplir con el m&iacute;nimo de facturaci&oacute;n asignado, sin importar el n&uacute;mero de Transacciones.&nbsp;El cliente deber&aacute; cumplir la meta de facturaci&oacute;n activando con un m&iacute;nimo de una transacci&oacute;n la categor&iacute;a asignada esa semana.&nbsp;</p>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{216}" paraid="1858834580">&nbsp;</p>

          <ol role="list" start="13">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="13" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{220}" paraid="309754023">&iquest;Cu&aacute;l es la&nbsp;Vigencia de&nbsp;la&nbsp;Campa&ntilde;a&nbsp;&ldquo;&Uacute;sala y Gana&rdquo;?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{8d14df0c-8073-4bbb-a1d6-7dcd4c96461c}{245}" paraid="1711230450">La vigencia de&nbsp;la&nbsp;Campa&ntilde;a&nbsp;inicia&nbsp;el&nbsp;01 de Noviembre de 2018&nbsp;y finaliza&nbsp;el&nbsp;&nbsp;31&nbsp;de Diciembre de 2018.&nbsp;&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{24}" paraid="241053276">&nbsp;</p>

          <ol role="list" start="14">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="14" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{29}" paraid="1311060748">&iquest;Cu&aacute;ntas veces puedo&nbsp;redimir&nbsp;Premios?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{44}" paraid="69481209">El Participante podr&aacute; redimir&nbsp;un premio del cat&aacute;logo por semana siempre que cumpla con su Meta Personal de Facturaci&oacute;n,&nbsp;y/o&nbsp;active la&nbsp;categor&iacute;a&nbsp;de comercio&nbsp;asignada,&nbsp;y&nbsp;siempre y cuando haya unidades disponibles del producto que desee redimir.&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{72}" paraid="237272597">&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{76}" paraid="2076452157">Las cantidades disponibles de los productos que aparecen en el cat&aacute;logo&nbsp;de premios, estar&aacute;n sujetas a la disponibilidad de inventario y hasta agotar existencia. Por ende, una vez se agote el producto ser&aacute; desmontado&nbsp;del cat&aacute;logo&nbsp;de premios, y el Participante &uacute;nicamente podr&aacute;&nbsp;canjear&nbsp;aquellos disponibles.&nbsp;&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{100}" paraid="785746560">&nbsp;</p>

          <ol role="list" start="15">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="15" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{104}" paraid="1362040156">&iquest;D&oacute;nde consulto el estado&nbsp;de&nbsp;mi&nbsp;Meta Personal de Facturaci&oacute;n&nbsp;y categor&iacute;a asignada?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{121}" paraid="319042765">El estado de las&nbsp;Categor&iacute;as&nbsp;y la asignaci&oacute;n de meta personal de&nbsp;facturaci&oacute;n&nbsp;las podr&aacute; consultar en:&nbsp;www.usalaygana.com,&nbsp;ingresando a&nbsp;&ldquo;MI&nbsp;META&rdquo;.&nbsp;&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{161}" paraid="303998434">&nbsp;</p>

          <ol role="list" start="16">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="16" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{165}" paraid="542486399">&iquest;D&oacute;nde consulto el estado de mis redenciones?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{172}" paraid="305814804">El estado de las redenciones las podr&aacute; consultar en:&nbsp;www.usalaygana.com&nbsp;ingresando a &ldquo;Historia de premios&rdquo;.&nbsp;&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{196}" paraid="1678865855">&nbsp;</p>

          <ol role="list" start="17">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="17" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{200}" paraid="1816156290">&iquest;D&oacute;nde puedo comprar para sumar a las Categor&iacute;as&nbsp;de comercio?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{211}" paraid="2073063783">El participante podr&aacute; comprar en cualquier establecimiento que disponga de un dat&aacute;fono que acepte&nbsp;Tarjetas&nbsp;D&eacute;bito&nbsp;y Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;y este dentro de la categor&iacute;a asignada esa semana.&nbsp;&nbsp;</p>

          <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{241}" paraid="1868265503">&nbsp;</p>

          <ol role="list" start="18">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="18" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{f88cc230-560a-4a31-b45f-89d13f245172}{245}" paraid="589052229">&iquest;A partir de qu&eacute; momento veo reflejada mis compras?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{3}" paraid="214816569">La informaci&oacute;n&nbsp;de cumplimiento de la meta&nbsp;se actualizar&aacute;&nbsp;en la Plataforma&nbsp;con un tiempo estimado de&nbsp;7&nbsp;d&iacute;as&nbsp;h&aacute;biles posterior a la fecha de&nbsp;fin de la semana.&nbsp;</p>

          <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{31}" paraid="1338400845">&nbsp;</p>

          <ol role="list" start="19">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="19" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{35}" paraid="141116791">&iquest;Necesito&nbsp;presentar alg&uacute;n cup&oacute;n o algo adicional para obtener el descuento?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{46}" paraid="1234866532">En caso que&nbsp;el participante reciba&nbsp;correos promocionando descuentos con alg&uacute;n aliado, podr&aacute;&nbsp;acceder al mismo pagando con&nbsp;sus&nbsp;Tarjetas&nbsp;D&eacute;bito&nbsp;y Cr&eacute;dito&nbsp;&nbsp;Mastercard&nbsp;de Banreservas. Si hubiere&nbsp;alg&uacute;n caso&nbsp;que se&nbsp;necesite&nbsp;un cup&oacute;n ser&aacute; indicado y entregado en la comunicaci&oacute;n&nbsp;que se le env&iacute;e al&nbsp;participante.&nbsp;</p>

          <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{96}" paraid="581804517">&nbsp;</p>

          <ol role="list" start="20">
            <li aria-setsize="-1" data-aria-level="1" data-aria-posinset="20" data-font="" data-leveltext="%1." data-listid="1" role="listitem">
            <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{100}" paraid="914446597">&iquest;Qu&eacute; pasa si se reversa una transacci&oacute;n?&nbsp;</p>
            </li>
          </ol>

          <p paraeid="{39133e1f-5804-422a-b7fa-0a441a568869}{109}" paraid="1237873864">&Uacute;nicamente se tendr&aacute;n en cuenta&nbsp;las&nbsp;transacciones exitosas con la Tarjeta D&eacute;bito&nbsp;y&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de Banreservas, por lo tanto, las transacciones reversadas no se contabilizaran para alcanzar la Meta Personal de Facturaci&oacute;n.&nbsp;</p>

        </div>
      </div>

    </div>

  </div>
</div>
