<div class="modal fade " id="modal-terminos" role="dialog">
  <div class="modal-dialog1">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button id="cerrarmodal" type="button" class="close">cerrar</button>
      </div>
      <div class="modal-body body-preg">
        <div class="terminos">
          <p>&nbsp;</p>

<ol>
<li data-aria-level="1" data-aria-posinset="1" data-font="Arial" data-leveltext="%1." data-listid="1"><strong>DEFINICIONES&nbsp;</strong>&nbsp;</li>
</ol>

<p><strong>PARTICIPANTES</strong><strong>:&nbsp;</strong>Son&nbsp;los titulares personas&nbsp;f&iacute;sicas&nbsp;con&nbsp;una cuenta de Ahorros o Corriente y que a su vez su medio de manejo&nbsp;de &eacute;stos&nbsp;sea a trav&eacute;s de Tarjeta D&eacute;bito&nbsp;y/o Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;de conformidad con&nbsp;lo establecido en el&nbsp;P&aacute;rrafo&nbsp;2 de la presente&nbsp;definici&oacute;n,&nbsp;&nbsp;en&nbsp;adelante &ldquo;LOS PARTICIPANTES&rdquo;,&nbsp;los&nbsp;cu&aacute;les&nbsp;ser&aacute;n&nbsp;seleccionados previamente por el Banco&nbsp;de Reservas de la Rep&uacute;blica Dominicana, Banco de Servicios M&uacute;ltiples, en adelante&nbsp;&ldquo;EL BANCO&rdquo;,&nbsp;o&nbsp;tercero autorizado por&nbsp;&eacute;ste&nbsp;&uacute;ltimo;&nbsp;comunic&aacute;ndoles&nbsp;su participaci&oacute;n a trav&eacute;s de correo electr&oacute;nico, SMS y/o Correo Directo.&nbsp;LOS PARTICIPANTES deber&aacute;n cumplir con los requisitos mencionados en&nbsp;el presente documento.&nbsp;</p>

<p>&nbsp;</p>

<p>No es necesario inscribirse&nbsp;para&nbsp;&nbsp;participar, pero s&iacute; deber&aacute;&nbsp;ingresar en la plataforma&nbsp;<a href="http://www.usalaygana.com/">www.usalaygana.com</a>&nbsp;para conocer&nbsp;su&nbsp;Meta Personal de Facturaci&oacute;n,&nbsp;conocer&nbsp;los&nbsp;diferentes&nbsp;premios, y redenci&oacute;n de&nbsp;Premios, lo cual podr&aacute; realizar en cualquier momento durante la Vigencia de la campa&ntilde;a&nbsp;siempre y cuando haya cumplido con la meta semanal establecida.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>1&deg;:&nbsp;</strong>Si el participante tiene m&aacute;s de una&nbsp;Tarjeta Debito y/o Cr&eacute;dito&nbsp;Banreservas&nbsp;Mastercard, las Transacciones que realice con todas&nbsp;estas&nbsp;contar&aacute;n para alcanzar su Meta Personal de Facturaci&oacute;n. No aplica para ninguna transacci&oacute;n que se realice en cajeros autom&aacute;ticos, ni tampoco&nbsp;para clientes persona jur&iacute;dica.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;2&ordm;:&nbsp;</strong>No participan las personas&nbsp;f&iacute;sicas&nbsp;que durante la vigencia de la presente&nbsp;campa&ntilde;a&nbsp;sus&nbsp;Tarjetas&nbsp;D&eacute;bito y/o&nbsp;Cr&eacute;dito&nbsp;Banreservas&nbsp;Mastercard&nbsp;se encuentren bloqueadas,&nbsp;en atraso,&nbsp;inactivas o canceladas por cualquier causa. Si&nbsp;EL BANCO&nbsp;verifica esta situaci&oacute;n con posterioridad al&nbsp;ingreso&nbsp;del Participante en la Plataforma, ser&aacute; excluido de la Campa&ntilde;a.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<ul>
<li data-aria-level="1" data-aria-posinset="1" data-font="Symbol" data-leveltext="" data-listid="9"><strong>MEDIOS DE PAGO:</strong>&nbsp;Para aplicaci&oacute;n de&nbsp;la presente&nbsp;campa&ntilde;a,&nbsp;se entender&aacute;n como medios de pago las&nbsp;Tarjetas&nbsp;D&eacute;bito y/o&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;</li>
</ul>

<ul>
<li data-aria-level="1" data-aria-posinset="1" data-font="Symbol" data-leveltext="" data-listid="9"><strong>FACTURACI&Oacute;N</strong>: Se entender&aacute; como el valor pagado&nbsp;en&nbsp;las compras realizadas&nbsp;a trav&eacute;s de POS, usando&nbsp;los medios de pago&nbsp;en los establecimientos de comercio.&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="2" data-font="Symbol" data-leveltext="" data-listid="9"><strong>POS:&nbsp;</strong>Se entender&aacute; como el dispositivo que permite realizar compras con los medios de manejo descritos anteriormente&nbsp;en los establecimientos de comercio.&nbsp;<strong>(</strong><strong>D</strong><strong>at&aacute;fonos)</strong>.&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="3" data-font="Symbol" data-leveltext="" data-listid="9"><strong>TRANSACCIONES:&nbsp;</strong>Son las compras realizadas por&nbsp;EL PARTICIPANTE&nbsp;en establecimos de comercio nacionales e internacionales con los medios de pago&nbsp;a trav&eacute;s del&nbsp;POS.&nbsp;&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="4" data-font="Symbol" data-leveltext="" data-listid="9"><strong>CATEGOR&Iacute;A DE COMERCIO</strong>: Es la agrupaci&oacute;n de Establecimientos de comercio&nbsp;que el Banco&nbsp;previamente asocie&nbsp;de acuerdo con su actividad econ&oacute;mica.&nbsp;&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="5" data-font="Symbol" data-leveltext="" data-listid="9"><strong>PLATAFORMA</strong>:&nbsp;Es la p&aacute;gina web&nbsp;<a href="http://www.usalaygana.com/">www.usalaygana.com</a>&nbsp;a trav&eacute;s de la cual los Participantes podr&aacute;n registrarse, consultar su Meta Personal de Facturaci&oacute;n,&nbsp;consultar&nbsp;&nbsp;todos&nbsp;los premios&nbsp;a redimir,&nbsp;redenci&oacute;n&nbsp;de&nbsp;premios&nbsp;del cat&aacute;logo,&nbsp;consultar las Categor&iacute;as de los comercios&nbsp;que participan en la&nbsp;Campa&ntilde;a&nbsp;e informaci&oacute;n de inter&eacute;s para el desarrollo de la&nbsp;campa&ntilde;a.&nbsp;&nbsp;</li>
</ul>

<ul>
<li data-aria-level="1" data-aria-posinset="1" data-font="Symbol" data-leveltext="" data-listid="9"><strong>META PERSONAL DE FACTURACI&Oacute;N</strong><strong>&nbsp;Y CATEGORIAS A ACTIVAR</strong>:&nbsp;&nbsp;</li>
</ul>

<p>Son&nbsp;establecidas&nbsp;por&nbsp;EL BANCO, de&nbsp;acuerdo con el&nbsp;comportamiento&nbsp; transaccional&nbsp;de cada PARTICIPANTE, previo al inicio de la &ldquo;Campa&ntilde;a&rdquo;. En&nbsp;caso&nbsp;&nbsp;de&nbsp;modificaci&oacute;n alguna,&nbsp;de la&nbsp;meta personal de facturaci&oacute;n&nbsp;y/ categor&iacute;as a activar&nbsp;ser&aacute; informado a trav&eacute;s de la plataforma arriba descrita o en otro medio y/o canal de acuerdo a la ley.&nbsp;&nbsp;</p>

<ul>
<li data-aria-level="1" data-aria-posinset="2" data-font="Symbol" data-leveltext="" data-listid="9"><strong>VIGENCIA</strong>:&nbsp;Es el t&eacute;rmino en que&nbsp;EL PARTICIPANTE&nbsp;podr&aacute; realizar las Transacciones para el cumplimiento de la Meta Personal de Facturaci&oacute;n; as&iacute; como el lapso de tiempo que tendr&aacute; para la redenci&oacute;n de los premios, siempre y cuando haya cumplido previamente con la Meta Personal de Facturaci&oacute;n.&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="3" data-font="Symbol" data-leveltext="" data-listid="9"><strong>PROP</strong><strong>&Oacute;</strong><strong>SITO:</strong>&nbsp;</li>
</ul>

<p>Incentivar&nbsp;los usos&nbsp;de los medios de pago premiando a los PARTICIPANTES que cumplan la meta de facturaci&oacute;n descrita en este documento y los t&eacute;rminos y condiciones previstos en la presente campa&ntilde;a.&nbsp;&nbsp;</p>

<ul>
<li data-aria-level="1" data-aria-posinset="1" data-font="Symbol" data-leveltext="" data-listid="9"><strong>PREMIOS</strong>: La Plataforma incluir&aacute; un cat&aacute;logo&nbsp;de&nbsp;premios&nbsp;sujetos a disponibilidad&nbsp;que podr&aacute;&nbsp;canjear&nbsp;el&nbsp;PARTICIPANTE&nbsp;cuando cumpla su Meta Personal de Facturaci&oacute;n.&nbsp;El cat&aacute;logo incluye la informaci&oacute;n de inventario y de unidades disponibles.&nbsp;El PARTICIPANTE podr&aacute; canjear un total de 8 premios durante la promoci&oacute;n,&nbsp;pero solo 1 premio por semana.&nbsp;[CANTIDAD DE PREMIOS&nbsp;EN INVENTARIO&nbsp;POR DEFINIR]&nbsp;</li>
</ul>

<p>&nbsp;</p>

<ol>
<li data-aria-level="1" data-aria-posinset="2" data-font="Arial" data-leveltext="%1." data-listid="1"><strong>T&Eacute;RMINOS Y CONDICIONES</strong>&nbsp;</li>
</ol>

<p><strong>PRIMERO:&nbsp;</strong><strong>REQUISITOS PARA PARTICIPAR&nbsp;</strong>-&nbsp;Teniendo en cuenta lo establecido al inicio del presente documento, los participantes deber&aacute;n cumplir los siguientes requisitos:&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<ol>
<li >Ser Titular como persona&nbsp;f&iacute;sica&nbsp;de m&iacute;nimo una Cuenta de Ahorros o Corriente y ser titular&nbsp;de&nbsp;cualquier medio de pago&nbsp;previsto&nbsp;&nbsp;en&nbsp;el presente documento.&nbsp;&nbsp;</li>

<li >EL PARTICIPANTE<strong>&nbsp;</strong>deber&aacute; estar al d&iacute;a&nbsp;o en estado normal&nbsp;en el pago de todos sus productos con el Banco&nbsp;&nbsp;</li>

<li >Deber&aacute; haber realizado transacciones de compras en establecimientos de comercio usando su&nbsp;Tarjeta Debito y/o&nbsp;Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas&nbsp;en POS (dat&aacute;fonos). Las transacciones que&nbsp;realice se contabilizar&aacute;n para alcanzar la Meta Personal de Facturaci&oacute;n.&nbsp;</li>

<li >Ser mayor de edad (18 a&ntilde;os cumplidos en adelante) y&nbsp;ser portador de Cedula de identidad y electora.&nbsp;</li>

<li >Participar&aacute;n los clientes que reciban un correo electr&oacute;nico, SMS y/o Correo Directo por parte de EL BANCO inform&aacute;ndoles sobre su participaci&oacute;n en la presente Campa&ntilde;a.&nbsp;</li>

<li >Durante toda la ejecuci&oacute;n de la presente Campa&ntilde;a el participante deber&aacute; ser cliente activo&nbsp;de EL&nbsp;BANCO.&nbsp;</li>

<li >En caso de que el tarjetahabiente no desee participar de ella, deber&aacute; comunicarse en los&nbsp;diferentes canales de servicio al cliente,&nbsp;la cual podr&aacute;n ser consultados en la p&aacute;gina web de EL BANCO.&nbsp;El PARTICIPANTE puede comunicarse a trav&eacute;s del n&uacute;mero de tel&eacute;fono:&nbsp;809-960-2121(banco debe llenar)&nbsp;</li>

<li >Toda la informaci&oacute;n relacionada con La Campa&ntilde;a ser&aacute; suministrada en idioma espa&ntilde;ol.&nbsp;&nbsp;</li>
</ol>

<p>&nbsp;</p>

<p><strong>SEGUNDO: VIGENCIA DE LA&nbsp;</strong><strong>CAMPA&Ntilde;A</strong><strong>.&nbsp;</strong>Las Transacciones que ser&aacute;n v&aacute;lidas para el cumplimiento de la Meta Personal de Facturaci&oacute;n, ser&aacute;n aquellas que realice&nbsp;El&nbsp;PARTICIPANTE&nbsp;entre el&nbsp;01&nbsp;de&nbsp;noviembre&nbsp;de 2018 a las 00:00 horas y el&nbsp;31&nbsp;de&nbsp;Diciembre&nbsp;de 2018 hasta las 23:59 horas&nbsp;t&eacute;rmino dentro del cual se deber&aacute; alcanzar&nbsp;la Meta Personal de Facturaci&oacute;n semanal y podr&aacute; canjear premios hasta el&nbsp;31 de Octubre&nbsp;de 2018&nbsp;hasta las 23:59 horas.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>TERCERO:</strong>&nbsp;<strong>TRANSACCIONES QUE CUENTAN PARA LA META</strong><strong>&nbsp;PERSONAL DE FACTURACI&Oacute;N</strong><strong>.</strong>&nbsp;Las Transacciones que ser&aacute;n contabilizadas y v&aacute;lidas para&nbsp;efectos de&nbsp;la&nbsp;Campa&ntilde;a&nbsp;ser&aacute;n &uacute;nicamente aquellas que&nbsp;realicen&nbsp;los&nbsp;PARTICIPANTES&nbsp;durante la vigencia de&nbsp;La&nbsp;Campa&ntilde;a&nbsp;&nbsp;con&nbsp;sus&nbsp;medios de pago&nbsp;en&nbsp;los&nbsp;establecimientos de comercio&nbsp;a trav&eacute;s de POS.&nbsp;Las Transacciones ser&aacute;n asignadas a una Categor&iacute;a&nbsp;de comercio&nbsp;seg&uacute;n la clasificaci&oacute;n que tenga el&nbsp;establecimiento de&nbsp;comercio&nbsp;asignada&nbsp;por&nbsp;EL BANCO.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;1.</strong>&nbsp; Cada Transacci&oacute;n ser&aacute; asignada a una Categor&iacute;a de comercio de acuerdo con el tipo de establecimiento de comercio donde&nbsp;el PARTICIPANTE realiz&oacute;&nbsp;la Transacci&oacute;n.&nbsp;&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;2.</strong>&nbsp; EL PARTICIPANTE, al registrarse en la Plataforma, autoriza a EL BANCO a hacer seguimiento de las transacciones que realice con sus medios de pago.&nbsp;</p>

<p><strong>CUARTO: MEC&Aacute;NICA</strong><strong>.</strong><strong>&nbsp;</strong>La mec&aacute;nica general de la campa&ntilde;a consiste en acumular&nbsp;facturaci&oacute;n&nbsp;por cada transacci&oacute;n realizada con sus&nbsp;medios de pago, en las condiciones establecidas en el presente documento. Para ello,&nbsp;EL BANCO&nbsp;asignar&aacute; una&nbsp;Meta&nbsp;Personal de Facturaci&oacute;n&nbsp;a&nbsp;los&nbsp;PARTICIPANTES&nbsp;previamente seleccionados&nbsp;e informados de su participaci&oacute;n en la&nbsp;Campa&ntilde;a, el cual, una vez cumplido lo anterior,&nbsp;EL PARTICIPANTE&nbsp;podr&aacute;&nbsp;canjear&nbsp;Premios&nbsp;del cat&aacute;logo&nbsp;publicado&nbsp;en la Plataforma.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>CUARTO:&nbsp;</strong><strong>SEGUIMIENTO Y CONOCIMIENTO DE LA META</strong><strong>.&nbsp;</strong>Para seguimiento y conocimiento de la campa&ntilde;a&nbsp;EL PARTICIPANTE&nbsp;deber&aacute;&nbsp;ingresar a&nbsp;la p&aacute;gina&nbsp;&nbsp;<a href="http://www.usalaygana.com/">www.usalaygana.com</a>&nbsp;con&nbsp;su n&uacute;mero de cedula y&nbsp;los &uacute;ltimos 4 d&iacute;gitos de su Tarjeta&nbsp;D&eacute;bito&nbsp;y/o Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas.&nbsp;En el evento,&nbsp;que EL PARTICIPANTE cambie sus datos de ingresos&nbsp;por cualquier motivo y requiera acceder a la plataforma deber&aacute; usar la misma informaci&oacute;n del primer ingreso.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>QUINTO</strong><strong>: PREMIOS.</strong>&nbsp;A cada&nbsp;Categor&iacute;a se le han asignado&nbsp;unos Premios determinados&nbsp;que el Participante podr&aacute; validar&nbsp;y redimir&nbsp;de acuerdo con&nbsp;el cat&aacute;logo.&nbsp;EL PARTICIPANTE&nbsp;acceder&aacute; al Premio que desee&nbsp;redimir&nbsp;de acuerdo&nbsp;al&nbsp;correspondiente cumplimiento&nbsp;de su&nbsp;Meta&nbsp;Personal de Facturaci&oacute;n&nbsp;y/o&nbsp;en&nbsp;las&nbsp;Categor&iacute;as.&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;1&deg;:&nbsp;</strong>Las caracter&iacute;sticas y cantidades de los Premios&nbsp;se podr&aacute;n&nbsp;conocer en&nbsp;la Plataforma, as&iacute; como&nbsp;la disponibilidad de los respectivos premios, la cual ser&aacute; actualizada por&nbsp;EL BANCO&nbsp;en tiempo real.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;2&deg;:&nbsp;</strong>El Participante&nbsp;podr&aacute;&nbsp;redimir&nbsp;un&nbsp;Premio&nbsp;cuando&nbsp;se active su Meta Personal de Facturaci&oacute;n&nbsp;semanal. Una vez&nbsp;cumpla&nbsp;la mec&aacute;nica establecida en&nbsp;los presentes t&eacute;rminos y condiciones&nbsp;tendr&aacute; derecho a&nbsp;canjear.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;3</strong><strong>&deg;:</strong><strong>&nbsp;</strong>El Participante podr&aacute; redimir&nbsp;8&nbsp;premios&nbsp;durante la campa&ntilde;a/ 1 por semana.&nbsp;Para redimir uno de los premios, debe ingresar al sitio web&nbsp;www.usalaygana.com&nbsp;y consultar en el cat&aacute;logo la disponibilidad de los&nbsp;mismos.&nbsp;EL PARTICIPANTE&nbsp;deber&aacute;&nbsp;verificar&nbsp;de tener&nbsp;un premio&nbsp;disponible&nbsp;para&nbsp;su&nbsp;redenci&oacute;n, &eacute;ste&nbsp;deber&aacute; hacerla efectiva&nbsp;para poder continuar con lo establecido en&nbsp;el&nbsp;numeral&nbsp;de PREMIOS.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;</strong><strong>4</strong><strong>&deg;:</strong>&nbsp;Ninguno de los Premios&nbsp;redimidos por&nbsp;EL PARTICIPANTE&nbsp;podr&aacute;&nbsp;ser&nbsp;sustituido por dinero en efectivo.&nbsp;EL PARTICIPANTE&nbsp;podr&aacute;&nbsp;redimir&nbsp;&uacute;nicamente los&nbsp;premios que est&eacute;n disponibles en el cat&aacute;logo, el cual cuenta con inventario&nbsp;a&nbsp;fin&nbsp;&nbsp;de&nbsp;que el Participante pueda monitorear las unidades disponibles.&nbsp;Las cantidades disponibles de los&nbsp;premios&nbsp;que aparecen en el&nbsp;cat&aacute;logo,&nbsp;estar&aacute;n sujetos&nbsp;a&nbsp;la disponibilidad de inventario y hasta agotar&nbsp;existencias. Por ende, una vez se agote el&nbsp;premio se deshabilitar&aacute; su&nbsp;redenci&oacute;n&nbsp; en&nbsp;el cat&aacute;logo y&nbsp;EL PARTICIPANTE&nbsp;que&nbsp;cumpla&nbsp;su Meta&nbsp;Personal de Facturaci&oacute;n&nbsp;semanal&nbsp;deber&aacute; elegir cualquier otro&nbsp;premio&nbsp;del cat&aacute;logo.&nbsp;Si por alguna raz&oacute;n en el momento de la solicitud el proveedor no cuenta con el premio seleccionado, ser&aacute; cambiado por uno de similares caracter&iacute;sticas.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;5</strong><strong>&deg;:</strong>&nbsp;EL BANCO&nbsp;no asume responsabilidad alguna por los reclamos presentados en cuanto a la calidad,&nbsp;cantidad,&nbsp;estado y proceso de entrega de los&nbsp;premios. Tampoco asume responsabilidad alguna por el agotamiento de las existencias. Los proveedores son responsables por las condiciones del mismo.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;6</strong><strong>&deg;:</strong>&nbsp;En caso de reclamo en cuanto&nbsp;a la calidad,&nbsp;cantidad,&nbsp;estado y proceso de entrega de los&nbsp;premios,&nbsp;EL BANCO&nbsp;trasladar&aacute; dicho reclamo al proveedor&nbsp;administrador del programa quien se encargar&aacute; de&nbsp;efectuar la investigaci&oacute;n correspondiente y dar respuesta&nbsp;AL PARTICIPANTE.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>P&Aacute;RRAFO</strong><strong>&nbsp;7&deg;</strong><strong>:&nbsp;</strong>Los premios no son transferibles a otras personas ni intercambiables por dinero ni por otro bien o incentivo diferente a los aprobados por EL BANCO. No se realizar&aacute;n reversiones ni devoluciones de los premios.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>SEXTA</strong><strong>:&nbsp;</strong>EL PARTICIPANTE&nbsp;podr&aacute; consultar la&nbsp;actualizaci&oacute;n&nbsp;de Transacciones&nbsp;en la Plataforma. Las Transacciones realizadas por los&nbsp;PARTICIPANTES, se ver&aacute;n&nbsp;reflejadas en la Plataforma&nbsp;hasta&nbsp;siete&nbsp;d&iacute;as h&aacute;biles&nbsp;despu&eacute;s&nbsp;del fin del reto de la semana.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>S&Eacute;PTIMA</strong><strong>:</strong><strong>&nbsp;</strong>EL BANCO&nbsp;podr&aacute; modificar en cualquier momento los presentes t&eacute;rminos y condiciones&nbsp;sin previo aviso.&nbsp;En el mismo sentido, EL BANCO&nbsp;podr&aacute;&nbsp;suspender&nbsp;de&nbsp;forma inmediata la presente&nbsp;Campa&ntilde;a, sin asumir responsabilidad alguna si se llegasen&nbsp;a detectar delitos, fraudes o cualquier irregularidad en la forma de participar, en la forma de realizar Transacciones,&nbsp;o en&nbsp;el comportamiento de alguno de los ganadores o si se presentara alguna circunstancia de fuerza mayor o caso fortuito que&nbsp;afecte los intereses o buena fe&nbsp;de&nbsp;EL BANCO. Esta circunstancia&nbsp;o alguna modificaci&oacute;n en los t&eacute;rminos y condiciones&nbsp;se comunicar&aacute;n&nbsp;p&uacute;blicamente&nbsp;a trav&eacute;s de la plataforma o&nbsp;en&nbsp;otro&nbsp;medio&nbsp;&nbsp;y/o canal de acuerdo con la ley.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>OCTAVA</strong><strong>:&nbsp;</strong>Podr&aacute;n participar en la campa&ntilde;a&nbsp;Colaboradores de&nbsp;EL BANCO, a excepci&oacute;n de los que integran la Gerencia de Medios de Pago, Direcci&oacute;n de Publicidad/Mercadeo&nbsp;y Direcci&oacute;n de Inteligencia y Soporte Comercial.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>NOVENA</strong><strong>:&nbsp;</strong><strong>COBERTURA GEOGRAFICA</strong><strong>.</strong>&nbsp;La Campa&ntilde;a estar&aacute; vigente en el territorio de la Rep&uacute;blica&nbsp;Dominicana,&nbsp;aplican&nbsp;compras en POS a nivel nacional&nbsp;e Internacional. No se consideran pagos de servicios ni transacciones en&nbsp;corresponsales&nbsp;bancarios o&nbsp;cajeros autom&aacute;ticos (ATM).&nbsp;</p>

<p>&nbsp;</p>

<p><strong>D&Eacute;CIMO:&nbsp;</strong>Este reglamento es totalmente independiente al&nbsp;de&nbsp;Tarjeta D&eacute;bito&nbsp;y Cr&eacute;dito&nbsp;Mastercard&nbsp;de&nbsp;Banreservas.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>D&Eacute;CIMO&nbsp;</strong><strong>PRIMERO</strong><strong>:&nbsp;</strong>El cliente conoce y acepta las condiciones establecidas en el presente documento y se adhiere a ellas para acceder a los premios asignados.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>D&Eacute;CIMO&nbsp;</strong><strong>SEGUNDO</strong><strong>:&nbsp;</strong><strong>USO DEL NOMBRE, IMAGEN Y PROPIEDAD IN</strong><strong>TELECTUAL DE LOS PARTICIPANTES.&nbsp;</strong>Los&nbsp;Participantes Y/O Ganadores&nbsp;de La Campa&ntilde;a autorizan expresamente la utilizaci&oacute;n, publicaci&oacute;n y reproducci&oacute;n, sin limitaci&oacute;n o restricci&oacute;n alguna, de su imagen, nombre, ciudad y fotograf&iacute;a en cualquier tipo de publicidad, promoci&oacute;n, publicaci&oacute;n, a trav&eacute;s de cualquier medio, con fines comerciales o informativos por parte de EL BANCO.&nbsp;LOS PARTICIPANTES Y/O GANADORES&nbsp;no tendr&aacute;n derechos a recibir contraprestaci&oacute;n alguna por los hechos descritos en el presente numeral.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p><strong>D&Eacute;CIMO&nbsp;</strong><strong>TERCERO</strong><strong>:&nbsp;</strong><strong>OBLIGACIONES DE EL BANCO:&nbsp;</strong>&nbsp;</p>

<p>&nbsp;</p>

<ul>
<li data-aria-level="1" data-aria-posinset="1" data-font="Symbol" data-leveltext="" data-listid="9">Clasificar a los&nbsp;PARTICIPANTES&nbsp;de La campa&ntilde;a, de acuerdo a la mec&aacute;nica dispuesta en este documento.&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="2" data-font="Symbol" data-leveltext="" data-listid="9">Contactar a los ganadores de la presente Campa&ntilde;a.&nbsp;&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="3" data-font="Symbol" data-leveltext="" data-listid="9">Velar por la entrega de los premios a los ganadores de La Campa&ntilde;a.&nbsp;Estimaci&oacute;n de entrega: 30 d&iacute;as&nbsp;laborables&nbsp;despu&eacute;s de realizar el canje en la web.&nbsp;&nbsp;</li>
<li data-aria-level="1" data-aria-posinset="4" data-font="Symbol" data-leveltext="" data-listid="9">Velar y garantizar por el cumplimiento de las condiciones y requisitos previstos en el presente documento para la elecci&oacute;n de los ganadores.&nbsp;</li>
</ul>

<p><strong>D&Eacute;CIMO&nbsp;</strong><strong>CUARTO:</strong><strong>&nbsp;</strong><strong>EXONERACI&Oacute;N DE RESPONSABILIDAD</strong><strong>.&nbsp;</strong>EL BANCO NO responder&aacute; por los da&ntilde;os y perjuicios sufridos por los ganadores de los premios que se entreguen en virtud de la presente campa&ntilde;a, ocasionados en el disfrute del premio, Se entiende que los ganadores act&uacute;an por su propia cuenta y riesgo.&nbsp;&nbsp;</p>

        </div>
      </div>

    </div>

  </div>
</div>
