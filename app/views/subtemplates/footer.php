<footer class="footer-inicial">
  <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-12 col-xs-12">
          <!-- <div class="col-md-2 col-sm-2 col-xs-2 footerlogo">
            <img src="images/logoFooter.png" alt="BanReservas">
          </div> -->
          <div class="col-md-12 col-sm-12 col-xs-12 nav-footer">
            <ul class="nav nav-tabs navbar-footer">
              <li role="presentation" class=""><a href="#" data-toggle="modal" data-target="#modal-preguntas">Preguntas Frecuentes</a></li>
              <li role="presentation"><a href="#" data-toggle="modal" data-target="#modal-terminos">T&eacute;rminos y condiciones</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12 copy">
          <p>Copyright 2018 Banreservas. Todo los derechos reservados.</p>
        </div>
      </div>
  </div>
</footer>
<!--//FOOTER -->

<!--Modales   -->
<?php include 'app/views/subtemplates/modal-informacion.php'; ?>
<?php include 'app/views/subtemplates/modal-preguntas.php'; ?>
<?php include 'app/views/subtemplates/modal-terminos.php'; ?>

<!--BOOTSTRAP CORE JAVA SCRIPT -->
<script src="<?php echo $url_sources ?>/js/jquery.min.js"> </script>
<script src="<?php echo $url_sources ?>/js/bootstrap.min.js"> </script>
<script src="<?php echo $url_sources ?>/js/owl.carousel.min.js"> </script>

<script Type="text/JavaScript" src="<?php echo $url_sources ?>/js/jquery.easing.min.js" > </script>
<!--Scroll  -->
<script src="<?php echo $url_sources ?>/js/smooth-scroll.min.js"> </script>
