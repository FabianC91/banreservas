<?php 
	class DatabaseChef{
		private $host;
		private $user;
		private $name_database;
		private $password;

		public function __construct(){
			$config = new Config('database');
			$this->set_connection_info($config->get_settings());
		}

		private function set_connection_info($info){
			$this->host = $info['host'];
			$this->user = $info['user'];
			$this->name_database = $info['name_database'];
			$this->password = $info['password'];
		}
	}
?>